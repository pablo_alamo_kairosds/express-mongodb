import 'dotenv/config';
import mongoose from 'mongoose';
import app from './server.js';

const PORT = process.env.PORT || 3000;
const uri = process.env.MONGODB_URL;

const bootstrap = async () => {

  // Connect to Mongo with mongoose
  await mongoose.connect(uri);

  // Run Express
  app.listen(PORT, () => console.log(`Server listening on port ${ PORT }`));

};

bootstrap();