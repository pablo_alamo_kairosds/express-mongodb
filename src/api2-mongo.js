'use strict';

import express from "express";
import { MongoClient, ObjectId } from "mongodb";
import morgan from "morgan";

/*===== MONGO SETTINGS =====*/

const uri = "mongodb://localhost:27017/customersDB";
const client = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true });

/*===== EXPRESS SETTINGS =====*/

const app = express();
const PORT = 3000;
// morgan logging requests
app.use(morgan(':method :url :status - :response-time ms - :date'));
// Convert every json body to JS object
app.use(express.json());

/*===== GLOBAL VARIABLES AND FUNCTIONS =====*/

let customers;

function toResponse(doc) {
  if (doc instanceof Array) {
    return doc.map(elem => toResponse(elem));
  } else {
    let { _id, ...ret } = doc;
    ret.id = doc._id.toString();
    return ret;
  }
}

/*===== ENDPOINTS =====*/

// app.method('path', handler)
app.get('/customers', async (req, res) => {
  const allCustomers = await customers.find().toArray();
  res.json(toResponse(allCustomers));
});

app.get('/customers/:id', async (req, res) => {
  const id = req.params.id;
  const customer = await customers.findOne({ _id: new ObjectId(id) });
  if (!customer) {
    res.status(404).send("Sorry can't find that customer");
  } else {
    res.json(toResponse(customer));
  }
});

app.post('/customers', async (req, res) => {
  const customer = req.body;

  // validation
  if (typeof customer.name !== 'string' || typeof customer.city !== 'string') {
    console.log(customer);
    res.status(400).send("Wrong body");
  } else {
    const newCustomer = {
      name: customer.name,
      city: customer.city
    };

    // save new customer
    await customers.insertOne(newCustomer);

    // return new customer
    res.json(toResponse(newCustomer));
  }
});

app.put('/customers/:id', async (req, res) => {
  const id = req.params.id;
  const customer = await customers.findOne({ _id: new ObjectId(id) });

  // validation
  if (!customer) {
    res.status(404).send("Sorry can't find that customer");
  } else {
    const customerReq = req.body;

    // validation
    if (typeof customerReq.name !== 'string' || typeof customerReq.city !== 'string') {
      res.status(400).send("Wrong body");
    } else {
      const newCustomer = {
        name: customerReq.name,
        city: customerReq.city
      };

      // update customer
      await customers.updateOne({ _id: new ObjectId(id) }, { $set: newCustomer });

      // return customer
      newCustomer._id = id;
      res.json(toResponse(newCustomer));
    }
  }
});

app.delete('/customers/:id', async (req, res) => {
  const id = req.params.id;
  const customer = await customers.findOne({ _id: new ObjectId(id) });

  if (!customer) {
    res.status(404).send("Sorry can't find that customer");
  } else {
    await customers.deleteOne({ _id: new ObjectId(id) });
    res.json(toResponse(customer));
  }

});

// Handle errors
app.use(function (err, req, res, next) {
  console.error(err.stack);
  res.status(500).send('Something broke!');
});

// Handle 404 because it is not an 'error', this will be at the bottom
app.use(function (req, res, next) {
  res.status(404).send('Sorry cant find that!');
});

/*===== CONNECTIONS =====*/

// Stablish connection with mongo
async function dbConnect() {
  await client.connect();
  await client.db("admin").command({ ping: 1 });
  console.log("Connected successfully to server");
  customers = client.db().collection("customers");
}

// Run Mongo and Express
async function main() {

  // Wait connection with Mongo
  await dbConnect();

  // Run Express
  app.listen(PORT, () => console.log(`Server listening on port ${ PORT }`));

}
main();