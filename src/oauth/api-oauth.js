import express from 'express';
import fs from 'fs';
import https from 'https';
import morgan from 'morgan';
import OAuth2Server, { Request, Response } from 'oauth2-server';
import model from './model.js';

// Clean terminal with nodemon :D
console.clear()

// Express
const app = express();
app.use(morgan(':method :url :status - :response-time ms - :date'));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// OAuth2
app.oauth = new OAuth2Server({
  model,
  accessTokenLifetime: 60 * 60,
  allowBearerTokensInQueryString: true
});

// Generate Token (the path must always begin with '/oauth/...')
app.all('/oauth/token', async (req, res) => {
  const request = new Request(req);
  const response = new Response(res);

  try {

    const token = await app.oauth.token(request, response);
    res.json(token);

  } catch (e) {
    res.status(e.code || 500).json(e);
  }

});

// Autentication
const authenticateRequest = async (req, res, next) => {
  const request = new Request(req);
  const response = new Response(res);

  try {

    await app.oauth.authenticate(request, response);
    next();

  } catch (e) {
    res.status(e.code || 500).json(e);
  }
};

// Private endpoint
app.get('/ads',
  authenticateRequest,
  (req, res) => {

    const sampleAds = [
      { id: 0, message: "Vendo moto", author: "Pepe" },
      { id: 1, message: "Compro TV", author: "Juan" },
      { id: 2, message: "Cambio manta", author: "Julián" }
    ];

    res.json(sampleAds);
  }
);

// Connections
const message = (protocol, port) => {
  console.log(`${ protocol } server started in port ${ port }`);
};
const options = {
  ky: fs.readFileSync('../../server.key'),
  cert: fs.readFileSync('../../server.cert')
};
app.listen(3000, () => message("http", 3000));
https.createServer(options, app).listen(3443, () => message("https", 3443));