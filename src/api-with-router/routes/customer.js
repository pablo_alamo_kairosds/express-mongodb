import express from "express";
import Customer from '../schemas/customer-schema.js';

const customerRouter = express.Router();

const toResponse = (doc) => {
  
  if (doc instanceof Array) {
    return doc.map(elem => toResponse(elem));
  }
  
  let ret = doc.toObject({ versionKey: false });
  ret.id = ret._id.toString();
  delete ret._id;
  
  return ret;

};

customerRouter.get('/', async (req, res) => {
  const allCustomers = await Customer.find().exec();
  return res.json(toResponse(allCustomers));
});

customerRouter.get('/:id', async (req, res) => {
  const id = req.params.id;
  const customer = await Customer.findById(id);

  if (!customer) {
    return res.status(404).send("Sorry can't find that customer");
  }
  return res.json(toResponse(customer));

});

customerRouter.post('/', async (req, res) => {
  const customer = req.body;

  // Check if already exists in DB
  const customerDB = await Customer.findOne({ name: customer.name, city: customer.city });
  if (customerDB) {
    return res.status(409).send("That customer already exists");
  }
  // validation
  if (typeof customer.name !== 'string' || typeof customer.city !== 'string') {
    return res.status(400).send("Bad Request: Invalid body");
  }

  const newCustomer = new Customer({
    name: customer.name,
    city: customer.city
  });

  // save new customer
  await newCustomer.save();

  // return new customer
  return res.json(toResponse(newCustomer));

});

customerRouter.put('/:id', async (req, res) => {
  const id = req.params.id;
  const customer = await Customer.findById(id);

  // validation
  if (!customer) {
    return res.status(404).send("Sorry can't find that customer");
  }
  const customerReq = req.body;

  // validation
  if (typeof customerReq.name !== 'string' || typeof customerReq.city !== 'string') {
    return res.status(400).send("Bad Request: Invalid body");
  }

  // Update fields in model
  customer.name = customerReq.name;
  customer.city = customerReq.city;

  // Save updated customer
  await customer.save();

  // return customer
  return res.json(toResponse(customer));

});

customerRouter.delete('/:id', async (req, res) => {

  const id = req.params.id;
  const customer = await Customer.findById(id);

  if (!customer) {
    return res.status(404).send("Sorry can't find that customer");
  }

  await Customer.findByIdAndDelete(id);
  return res.json(toResponse(customer));

});

export default customerRouter;