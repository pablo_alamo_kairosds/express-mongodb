/**
* Configuration.
*/

const config = {
  clients: [{
    id: 'application',	// TODO: Needed by refresh_token grant, because there is a bug at line 103 in https://github.com/oauthjs/node-oauth2-server/blob/v3.0.1/lib/grant-types/refresh-token-grant-type.js (used client.id instead of client.clientId)
    clientId: 'application',
    clientSecret: 'secret',
    grants: [
      'password',
      'refresh_token'
    ],
    redirectUris: []
  }],
  confidentialClients: [{
    clientId: 'confidentialApplication',
    clientSecret: 'topSecret',
    grants: [
      'password',
      'client_credentials'
    ],
    redirectUris: []
  }],
  tokens: [],
  users: [{
    username: 'pedroetb',
    password: 'password'
  }]
};

/**
* Dump the memory storage content (for debug).
*/

var dump = () => {

  console.log('clients', config.clients);
  console.log('confidentialClients', config.confidentialClients);
  console.log('tokens', config.tokens);
  console.log('users', config.users);
};

/*
* Methods used by all grant types.
*/

const getAccessToken = (token) => {

  const accessToken = config.tokens.find(savedToken =>
    savedToken.accessToken === token);

  return accessToken;
};

const getClient = (clientId, clientSecret) => {

  const client = config.clients.find(client =>
    client.clientId === clientId && client.clientSecret === clientSecret);

  const confidentialClient = config.confidentialClients.find(client =>
    client.clientId === clientId && client.clientSecret === clientSecret);

  return client || confidentialClient;
};

const saveToken = (token, { clientId }, { username }) => {

  token.client = {
    id: clientId
  };

  token.user = {
    username
  };

  config.tokens.push(token);

  return token;
};

/*
* Method used only by password grant type.
*/

const getUser = (username, password) => {

  const user = config.users.find(user =>
    user.username === username && user.password === password);

  return user;
};

/*
* Method used only by client_credentials grant type.
*/

const getUserFromClient = (client) => {

  const clients = config.confidentialClients.filter(savedClient =>
    savedClient.clientId === client.clientId && savedClient.clientSecret === client.clientSecret);

  return clients.length;
};

/*
* Methods used only by refresh_token grant type.
*/

const getRefreshToken = (refreshToken) => {

  const newRefreshToken = config.tokens.find(savedToken =>
    savedToken.refreshToken === refreshToken);

  if (!newRefreshToken) {
    return;
  }

  return newRefreshToken;
};

const revokeToken = (token) => {

  config.tokens = config.tokens.filter(savedToken =>
    savedToken.refreshToken !== token.refreshToken);

  const revokedTokensFound = config.tokens.filter(savedToken =>
    savedToken.refreshToken === token.refreshToken);

  return !revokedTokensFound.length;
};

/**
* Export model definition object.
*/

export default {
  getAccessToken,
  getClient,
  saveToken,
  getUser,
  getUserFromClient,
  getRefreshToken,
  revokeToken
};