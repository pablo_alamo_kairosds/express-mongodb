import supertest from "supertest";
import app from "../server.js";

const request = supertest(app);

test('gets the customers endpoint', async () => {

    const response = await request.get('/customers');
        // .expect('Content-type', /json/)
        // .expect(200)

    console.log(response.header);

    expect(response.body.length).toBe(3)

})