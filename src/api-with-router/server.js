import express from "express";
import morgan from "morgan";
import customerRouter from './routes/customer.js';

console.clear();

/*===== EXPRESS SETTINGS =====*/

const app = express();

// ---> app.use(middleware)
// ---> app.use((err, req, res, next) => {})

// morgan logging requests
app.use(morgan(':method :url :status - :response-time ms - :date'));
// Parses every json body to JS object
app.use(express.json());
// Parses every text body to JS object
app.use(express.text());
// Load every routes in router
app.use('/customers', customerRouter);

/*===== HANDLE ERRORS =====*/

// 500: server is not working
app.use((err, req, res, next) => {
  console.error(err.stack);
  return res.status(500).send('Something broke!');
});

// 404: because it is not an 'error', this will be at the bottom
app.use((req, res) => {
  return res.status(404).send('Sorry cant find that!');
});

export default app;