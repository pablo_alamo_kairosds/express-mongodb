'use strict';

import express from "express";
import morgan from "morgan";

/*===== EXPRESS SETTINGS =====*/

const app = express();
const PORT = 3000;
// morgan logging requests
app.use(morgan(':method :url :status - :response-time ms - :date'));
// Convert every json body to JS object
app.use(express.json());

/*===== GLOBAL VARIABLES AND FUNCTIONS =====*/

let customers = new Map();

let uid = 0;
const getuid = () => {
  uid++;
  return uid.toString();
}

/*===== ENDPOINTS =====*/

// app.method('path', handler)
app.get('/customers', (req, res) => {
  const allCustomers = [...customers.values()];
  res.json(allCustomers);
});

app.get('/customers/:id', (req, res) => {
  const id = req.params.id;
  const customer = customers.get(id);
  if (!customer) {
    res.status(404).send("Sorry can't find that customer");
  } else {
    res.json(customer);
  }
});

app.post('/customers', (req, res) => {
  const customer = req.body;

  // if usuario exste, error de conflicto: 409

  // validation
  if (typeof customer.name !== 'string' || typeof customer.city !== 'string') {
    console.log(customer);
    res.status(400).send("Wrong body");
  } else {
    const newCustomer = {
      id: getuid(),
      name: customer.name,
      city: customer.city
    };

    // save new customer
    customers.set(newCustomer.id, newCustomer);

    // return new customer
    res.json(newCustomer);
  }
});

app.put('/customers/:id', (req, res) => {
  const id = req.params.id;
  const customer = customers.get(id);

  // validation
  if (!customer) {
    res.status(404).send("Sorry can't find that customer");
  } else {
    const customerReq = req.body;

    // validation
    if (typeof customerReq.name !== 'string' || typeof customerReq.city !== 'string') {
      res.status(400).send("Wrong body");
    } else {
      const newCustomer = {
        id,
        name: customerReq.name,
        city: customerReq.city
      };

      // update customer
      customers.set(newCustomer.id, newCustomer);

      // return customer
      res.json(newCustomer);
    }
  }
});

app.delete('/customers/:id', (req, res) => {
  const id = req.params.id;
  const customer = customers.get(id);

  if (!customer) {
    res.status(404).send("Sorry can't find that customer");
  } else {
    customers.delete(id);
    res.json(customer);
  }
});

// Handle errors
app.use(function (err, req, res, next) {
  console.error(err.stack);
  res.status(500).send('Something broke!');
});

// Handle 404 because it is not an 'error', this will be at the bottom
app.use(function (req, res, next) {
  res.status(404).send('Sorry cant find that!');
});

/*===== CONNECTIONS =====*/

app.listen(PORT, () => console.log(`Server listening on port ${ PORT }`));