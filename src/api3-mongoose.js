'use strict';

import 'dotenv/config';
import express from "express";
import mongoose from "mongoose";
import morgan from "morgan";

/*===== MONGOOSE SETTINGS =====*/

// const uri = process.env.MONGODB_URL;
const uri = "mongodb://localhost:27017/customersDB";

/*===== EXPRESS SETTINGS =====*/
const app = express();
const PORT = process.env.PORT || 3000;
// morgan logging requests
app.use(morgan(':method :url :status - :response-time ms - :date'));
// Convert every json body to JS object
app.use(express.json());

/*===== GLOBAL VARIABLES AND FUNCTIONS =====*/

let Customer;

function toResponse(doc) {
  if (doc instanceof Array) {
    return doc.map(elem => toResponse(elem));
  } else {
    let ret = doc.toObject({ versionKey: false })
    ret.id = ret._id.toString();
    delete ret._id;
    return ret;
  }
}

/*===== ENDPOINTS =====*/

// app.method('path', handler)
app.get('/customers', async (req, res) => {
  const allCustomers = await Customer.find().exec();
  res.json(toResponse(allCustomers));
});

app.get('/customers/:id', async (req, res) => {
  const id = req.params.id;
  const customer = await Customer.findById(id);
  if (!customer) {
    res.status(404).send("Sorry can't find that customer");
  } else {
    res.json(toResponse(customer));
  }
});

app.post('/customers', async (req, res) => {
  const customer = req.body;

  // validation
  if (typeof customer.name !== 'string' || typeof customer.city !== 'string') {
    res.status(400).send("Wrong body");
  } else {
    const newCustomer = new Customer({
      name: customer.name,
      city: customer.city
    });

    // save new customer
    await newCustomer.save();

    // return new customer
    res.json(toResponse(newCustomer));
  }
});

app.put('/customers/:id', async (req, res) => {
  const id = req.params.id;
  const customer = await Customer.findById(id);

  // validation
  if (!customer) {
    res.status(404).send("Sorry can't find that customer");
  } else {
    const customerReq = req.body;

    // validation
    if (typeof customerReq.name !== 'string' || typeof customerReq.city !== 'string') {
      res.status(400).send("Wrong body");
    } else {

      // Update fields in model
      customer.name = customerReq.name;
      customer.city = customerReq.city;

      // Save updated customer
      await customer.save();

      res.json(toResponse(customer));
    }
  }
});

app.delete('/customers/:id', async (req, res) => {
  const id = req.params.id;
  const customer = await Customer.findById(id);

  if (!customer) {
    res.status(404).send("Sorry can't find that customer");
  } else {
    await Customer.findByIdAndDelete(id);
    res.json(toResponse(customer));
  }

});

// Handle errors
app.use(function (err, req, res, next) {
  console.error(err.stack);
  res.status(500).send('Something broke!');
});

// Handle 404 because it is not an 'error', this will be at the bottom
app.use(function (req, res, next) {
  res.status(404).send('Sorry cant find that!');
});

/*===== CONNECTIONS =====*/

// Run Express
async function main() {

  // Connect to Mongo
  await mongoose.connect(uri);

  // Create mongoose Schema
  const customerSchema = new mongoose.Schema({
    name: String,
    city: String
  });
  Customer = mongoose.model('Customer', customerSchema);

  // Run Express
  app.listen(PORT, () => console.log(`Server listening on port ${ PORT }`));

}
main();