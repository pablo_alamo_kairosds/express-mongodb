
import bcrypt from 'bcrypt';
import 'dotenv/config';
import mongoose from "mongoose";

const uri = process.env.MONGODB_URL;

let User;

const createSampleUsers = async () => {
  await addUser('user1', 'pass1');
  await addUser('user2', 'pass2');
  await addUser('user3', 'pass3');
}

const addUser = async (username, password) => {
  const passwordHash = await bcrypt.hash(password, bcrypt.genSaltSync(8), null);

  let user = await User.findOne({ username }).exec();

  if (!user) {
    user = new User({ username, passwordHash });
  } else {
    user.passwordHash = passwordHash;
  }

  await user.save();
}

export const initMongo = async () => {

  await mongoose.connect(uri);

  const userSchema = new mongoose.Schema({
    username: String,
    passwordHash: String
  });
  User = mongoose.model('User', userSchema);

  createSampleUsers();
};

export const findUser = async (username) => {
  return await User.findOne({ username }).exec();
};

export const verifyPassword = async (user, password) => {
  return await bcrypt.compare(password, user.passwordHash);
};