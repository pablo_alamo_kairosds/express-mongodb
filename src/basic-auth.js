const username = "pablodevs";
const password = "N0T3L4D1g0";
const credentials = `${username}:${password}`;
let buff = Buffer.from(credentials).toString('base64');

const headers = {
  "Authorization": 'Basic ' + buff
};

console.log("credentials:", username, password);
console.log(headers["Authorization"]);