import { createConnection } from 'mysql2/promise';

async function main() {

  const client = await createConnection({
    host: 'localhost',
    user: 'root',
    password: 'pass',
    database: 'customersDB'
  });

  console.log("Connected to MySQL");

  const [rows1, fields1] = await client.execute(
    'INSERT INTO customers SET firstName = ?, lastName = ?',
    ['Jack', 'Bauer']
  );

  const [{ insertId }] = await client.execute(
    'INSERT INTO customers SET firstName = ?, lastName = ?',
    ['Jack', 'Bauer']
  );

  const id = insertId;
  const [rows2, fields2] = await client.execute(
    'SELECT * FROM customers WHERE id = ?',
    [id]
  );

  console.log("Customer inserted");
  console.log("rows", rows1);
  console.log("fields", fields1);
  console.log("insertId", insertId);
  console.log("queried id", rows2[0]);

  await client.close();

  console.log("Connection closed");
}

main();
