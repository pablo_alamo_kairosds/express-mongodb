import 'dotenv/config';
import express from 'express';
import fs from 'fs';
import https from 'https';
import jwt from 'jsonwebtoken';
import morgan from 'morgan';
import passport from 'passport';
import { BasicStrategy } from 'passport-http';
import { ExtractJwt, Strategy as JwtStrategy } from 'passport-jwt';
import { findUser, initMongo, verifyPassword } from './users.js';

// ENV
const HTTPS_PORT = process.env.HTTPS_PORT;
const HTTP_PORT = process.env.HTTP_PORT;
const SECRET_KEY = process.env.HTTP_PORT;

// Express
const app = express();
app.use(express.json());
app.use(passport.initialize());
app.use(morgan(':method :url :status - :response-time ms - :date'));

// Passport Basic Auth
async function basicVerify(username, password, done) {

  const user = await findUser(username);

  if (!user) {
    return done(null, false, { message: 'User not found' });
  }

  if (await verifyPassword(user, password)) {
    return done(null, user);
  } else {
    return done(null, false, { message: 'Incorrect password' });
  }
}

passport.use(new BasicStrategy(basicVerify));

// Endpoints
app.post('/login',
  passport.authenticate('basic', { session: false }),
  (req, res) => {
    const { username } = req.user;

    const options = { expiresIn: 120 };
    const token = jwt.sign({ username }, SECRET_KEY, options);

    return res.status(200).json({ message: "Auth Passed", token });
  }
);

// JWT Auth
const jwtOptions = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: SECRET_KEY
};

passport.use(new JwtStrategy(jwtOptions, async (payload, done) => {
  const user = await findUser(payload.username);

  if (user) {
    return done(null, user);
  } else {
    return done(null, false, { message: 'User not found' });
  }
}));

app.get('/ads',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    const sampleAds = [
      { id: 1, message: "Vendo moto", author: "Pepe" },
      { id: 2, message: "Compro TV", author: "Juan" },
      { id: 3, message: "Cambio manta", author: "Julián" }
    ];

    res.json(sampleAds);
  });

// Connections
initMongo();

const message = (protocol, port) => {
  console.log(`${ protocol } server started in port ${ port }`);
};
const options = {
  ky: fs.readFileSync('server.key'),
  cert: fs.readFileSync('server.cert')
};
app.listen(HTTP_PORT, () => message("http", HTTP_PORT))
https.createServer(options, app).listen(HTTPS_PORT, () => message("https", HTTPS_PORT));