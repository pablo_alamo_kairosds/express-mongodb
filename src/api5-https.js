import express from 'express';
import fs from 'fs';
import http from 'http';
import https from 'https';

const app = express();

const httpsPORT = 3443;
const httpPORT = 3000;

const options = {
  ky: fs.readFileSync('server.key'),
  cert: fs.readFileSync('server.cert')
};
const message = (protocol, port) => {
  console.log(`${protocol} server started in port ${port}`);
};

app.get('/', async (req, res) => {
  console.log("Hola consola");
  res.json("Hola Front");
});

http.createServer(app).listen(httpPORT, () => message("http", httpPORT));
https.createServer(options, app).listen(httpsPORT, () => message("https", httpsPORT));