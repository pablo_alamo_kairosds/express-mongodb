import mongoose from "mongoose";

// Create mongoose Schema
const customerSchema = new mongoose.Schema({
  name: String,
  city: String
});
const CustomerModel = mongoose.model('Customer', customerSchema);

export default CustomerModel