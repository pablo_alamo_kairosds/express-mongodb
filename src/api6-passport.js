import express from 'express';
import fs from 'fs';
import http from 'http';
import https from 'https';
import passport from 'passport';
import { BasicStrategy } from 'passport-http';

const app = express();
app.use(express.json());

const httpsPORT = 3443;
const httpPORT = 3000;

// Passport
function verify(username, password, done) {
  if (username === 'admin' && password === 'pass') {
    return done(null, { username, password });
  } else {
    return done(null, false, { message: 'Incorrect username or password' });
  }
}

passport.use(new BasicStrategy(verify));
app.use(passport.initialize());

app.get('/', async (req, res) => {
  console.log("Hola consola");
  res.json("Hola Front");
});

app.get('/ads',
  passport.authenticate('basic', { session: false }),
  (req, res) => {

    console.log('Logged user:', req.user);

    const sampleAds = [
      { id: 0, message: "Vendo moto", author: "Pepe" },
      { id: 1, message: "Compro TV", author: "Juan" },
      { id: 2, message: "Cambio manta", author: "Julián" }
    ];

    res.json(sampleAds);
  });

// Connections
const message = (protocol, port) => {
  console.log(`${ protocol } server started in port ${ port }`);
};
const options = {
  ky: fs.readFileSync('server.key'),
  cert: fs.readFileSync('server.cert')
};
http.createServer(app).listen(httpPORT, () => message("http", httpPORT));
https.createServer(options, app).listen(httpsPORT, () => message("https", httpsPORT));