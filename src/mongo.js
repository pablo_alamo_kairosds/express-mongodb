import { MongoClient } from "mongodb";

// Connection URI
const uri = "mongodb://localhost:27017/customersDB";

// Create a new MongoClient
const client = new MongoClient(uri);
async function run() {
  try {
    // Connect the client to the server
    await client.connect();
    // Establish and verify connection
    await client.db("admin").command({ ping: 1 });
    console.log("Connected successfully to server");

    const customers = client.db().collection("customers");

    const { insertedId } = await customers.insertOne({
      name: "Dani",
      age: 25
    });
    console.log("New customer inserted: " + insertedId);

  } finally {
    // Ensures that the client will close when you finish/error
    await client.close();
  }
}
run().catch(console.dir);